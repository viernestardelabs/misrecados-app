package com.viernestardelabs.misrecados.client.ui;

import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import javax.inject.Inject;

/** http://css-tricks.com/replicating-google-hangouts-chat/ */
public class HangoutTimeline extends Composite {
    interface MyUiBinder extends UiBinder<HTMLPanel, HangoutTimeline> {}

    @Inject
    public HangoutTimeline(MyUiBinder myUiBinder) {
        initWidget(myUiBinder.createAndBindUi(this));
    }
}
