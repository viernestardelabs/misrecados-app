package com.viernestardelabs.misrecados.client.ui;

import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import javax.inject.Inject;

/** http://tympanus.net/Blueprints/VerticalTimeline/ */
public class VerticalTimeline extends Composite {
    interface MyUiBinder extends UiBinder<Widget, VerticalTimeline> {}

    @Inject
    public VerticalTimeline(MyUiBinder uiBinder) {
        initWidget(uiBinder.createAndBindUi(this));
    }
}
