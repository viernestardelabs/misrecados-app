package com.viernestardelabs.misrecados.client;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;
import com.google.inject.Inject;
import com.viernestardelabs.misrecados.client.activities.about.AboutActivity;
import com.viernestardelabs.misrecados.client.activities.about.AboutPlace;
import com.viernestardelabs.misrecados.client.activities.erranddetail.ErrandDetailActivity;
import com.viernestardelabs.misrecados.client.activities.erranddetail.ErrandDetailPlace;
import com.viernestardelabs.misrecados.client.activities.errandlist.ErrandListActivity;
import com.viernestardelabs.misrecados.client.activities.errandlist.ErrandListPlace;
import com.viernestardelabs.misrecados.client.activities.navigation.HomePlace;
import com.viernestardelabs.misrecados.client.activities.navigation.NavigationActivity;
import com.viernestardelabs.misrecados.client.activities.profile.ProfileActivity;
import com.viernestardelabs.misrecados.client.activities.profile.ProfilePlace;

import javax.inject.Singleton;

@Singleton
public class PhoneActivityMapper implements ActivityMapper {

    private final NavigationActivity navigationActivity;
    private final ErrandListActivity errandListActivity;
    private final ErrandDetailActivity errandDetailActivity;
    private final ProfileActivity profileActivity;
	private final AboutActivity aboutActivity;

    @Inject
    public PhoneActivityMapper(
            final ErrandListActivity errandListActivity,
            final ErrandDetailActivity errandDetailActivity,
            final ProfileActivity profileActivity,
            final NavigationActivity navigationActivity, 
            final AboutActivity aboutActivity) {
        this.errandListActivity = errandListActivity;
        this.errandDetailActivity = errandDetailActivity;
        this.profileActivity = profileActivity;
        this.navigationActivity = navigationActivity;
		this.aboutActivity = aboutActivity;
    }

    @Override
    public Activity getActivity(Place place) {
        if (place instanceof HomePlace) return navigationActivity;
        if (place instanceof ErrandListPlace) return errandListActivity;
        if (place instanceof ErrandDetailPlace) return errandDetailActivity;
        if (place instanceof ProfilePlace) return profileActivity;
        if (place instanceof AboutPlace) return aboutActivity;
        return navigationActivity;
    }
}
