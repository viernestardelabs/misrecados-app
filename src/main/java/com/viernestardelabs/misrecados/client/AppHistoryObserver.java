package com.viernestardelabs.misrecados.client;

import static com.viernestardelabs.misrecados.client.event.ActionNames.BACK;

import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.History;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.googlecode.mgwt.dom.client.event.mouse.HandlerRegistrationCollection;
import com.googlecode.mgwt.mvp.client.history.HistoryHandler;
import com.googlecode.mgwt.mvp.client.history.HistoryObserver;
import com.googlecode.mgwt.ui.client.MGWT;
import com.viernestardelabs.misrecados.client.activities.errandlist.ErrandListPlace;
import com.viernestardelabs.misrecados.client.activities.navigation.HomePlace;
import com.viernestardelabs.misrecados.client.event.ActionEvent;

public class AppHistoryObserver implements HistoryObserver {

    @Override
    public void onPlaceChange(Place place, HistoryHandler handler) {
    }

    @Override
    public void onHistoryChanged(Place place, HistoryHandler handler) {
    }

    @Override
    public void onAppStarted(Place place, HistoryHandler historyHandler) {
        if (MGWT.getOsDetection().isPhone()) {
            historyHandler.replaceCurrentPlace(new HomePlace());
        } else { // isTablet
            historyHandler.replaceCurrentPlace(new HomePlace());
            historyHandler.pushPlace(new ErrandListPlace());
        }

    }

    @Override
    public HandlerRegistration bind(EventBus eventBus, final HistoryHandler historyHandler) {
        HandlerRegistrationCollection collection = new HandlerRegistrationCollection();
        collection.addHandlerRegistration(ActionEvent.register(eventBus, BACK, new ActionEvent.Handler() {
            @Override
            public void onAction(ActionEvent event) {
                History.back();
            }
        }));
        return collection;
    }

}
