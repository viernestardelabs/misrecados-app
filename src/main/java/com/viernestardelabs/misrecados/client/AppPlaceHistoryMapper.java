package com.viernestardelabs.misrecados.client;

import com.google.gwt.place.shared.PlaceHistoryMapper;
import com.google.gwt.place.shared.WithTokenizers;
import com.viernestardelabs.misrecados.client.activities.about.AboutPlace;
import com.viernestardelabs.misrecados.client.activities.erranddetail.ErrandDetailPlace;
import com.viernestardelabs.misrecados.client.activities.errandlist.ErrandListPlace;
import com.viernestardelabs.misrecados.client.activities.navigation.HomePlace;
import com.viernestardelabs.misrecados.client.activities.profile.ProfilePlace;

@WithTokenizers({
        ErrandDetailPlace.ErrandDetailPlaceTokenizer.class,
        ErrandListPlace.ErrandListPlaceTokenizer.class,
        HomePlace.HomePlaceTokenizer.class,
        ProfilePlace.ProfilePlaceTokenizer.class,
        AboutPlace.AboutPlaceTokenizer.class
})
public interface AppPlaceHistoryMapper extends PlaceHistoryMapper {}
