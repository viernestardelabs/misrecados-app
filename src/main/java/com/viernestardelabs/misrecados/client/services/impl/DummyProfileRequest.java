package com.viernestardelabs.misrecados.client.services.impl;

import com.viernestardelabs.misrecados.client.services.ProfileRequest;
import com.viernestardelabs.misrecados.shared.domain.Profile;

public class DummyProfileRequest implements ProfileRequest {
    private final static ProfileRequest INSTANCE = new DummyProfileRequest();

    public static ProfileRequest instance() {
        return INSTANCE;
    }

    private static Profile profile;

    private DummyProfileRequest() {
        saveProfile(Profile.create("User", "Me", 0));
    }

    @Override
    public Profile getMeProfile() {
        return profile;
    }

    @Override
    public Profile getProfile(Integer userId) {
        return profile;
    }

    @Override
    public void saveProfile(Profile profile) {
        DummyProfileRequest.profile = profile;
    }

}
