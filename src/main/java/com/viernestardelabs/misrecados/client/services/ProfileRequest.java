package com.viernestardelabs.misrecados.client.services;

import com.viernestardelabs.misrecados.shared.domain.Profile;

public interface ProfileRequest {

    public Profile getMeProfile();

    public Profile getProfile(Integer userId);

    public void saveProfile(Profile profile);
}
