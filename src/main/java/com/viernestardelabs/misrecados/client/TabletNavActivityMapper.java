package com.viernestardelabs.misrecados.client;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;
import com.google.inject.Inject;
import com.viernestardelabs.misrecados.client.activities.navigation.NavigationActivity;
import javax.inject.Singleton;

@Singleton
public class TabletNavActivityMapper implements ActivityMapper {

    private final NavigationActivity navigationActivity;

    @Inject
    public TabletNavActivityMapper(final NavigationActivity navigationActivity) {
        this.navigationActivity = navigationActivity;
    }

    @Override
    public Activity getActivity(Place place) {
        return navigationActivity;
    }
}
