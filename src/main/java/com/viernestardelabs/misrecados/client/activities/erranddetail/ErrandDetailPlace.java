package com.viernestardelabs.misrecados.client.activities.erranddetail;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class ErrandDetailPlace extends Place {
	public static class ErrandDetailPlaceTokenizer implements PlaceTokenizer<ErrandDetailPlace> {
		@Override
		public ErrandDetailPlace getPlace(String token) {
			return new ErrandDetailPlace();
		}

		@Override
		public String getToken(ErrandDetailPlace place) {
			return "";
		}
	}

    @Override
    public String toString() {
        return "errand-detail";
    }
}
