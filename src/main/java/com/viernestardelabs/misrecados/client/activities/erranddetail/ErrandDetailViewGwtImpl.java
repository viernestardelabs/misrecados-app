package com.viernestardelabs.misrecados.client.activities.erranddetail;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.googlecode.mgwt.ui.client.MGWT;
import com.viernestardelabs.misrecados.client.activities.DetailViewGwtImpl;
import com.viernestardelabs.misrecados.client.activities.errandlist.ErrandListView.Content;
import com.viernestardelabs.misrecados.client.bundle.ClientResources;

public class ErrandDetailViewGwtImpl extends DetailViewGwtImpl implements ErrandDetailView {
	
	public final DetailPanel detailPanel;
	
	public ErrandDetailViewGwtImpl() {
		super();
		
		FlowPanel container = new FlowPanel();
		
		ClientResources resources = GWT.create(ClientResources.class);
		
		detailPanel = new DetailPanel();
		
		Image image = new Image(resources.mapImage());
		image.setWidth("100%");
		container.add(image);
		container.add(detailPanel);
		
		scrollPanel.setScrollingEnabledX(false);
        scrollPanel.setScrollingEnabledY(true);
        scrollPanel.setWidget(container);
        // workaround for android formFields jumping around when using -webkit-transform
        scrollPanel.setUsePos(MGWT.getOsDetection().isAndroid());
	}
	
	@Override
	public void setContentDetail(Content detail) {
		detailPanel.refreshData(detail);
	}
}
