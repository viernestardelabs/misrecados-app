package com.viernestardelabs.misrecados.client.activities.about;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class AboutPlace extends Place {
	public static class AboutPlaceTokenizer implements PlaceTokenizer<AboutPlace> {

		@Override
		public AboutPlace getPlace(String token) {
			return new AboutPlace();
		}

		@Override
		public String getToken(AboutPlace place) {
			return "";
		}

	}
	
	@Override
    public int hashCode() {
        return AboutPlace.class.hashCode();
    }

    @Override
    public boolean equals(Object other) {
        return other == this || other instanceof AboutPlace;
    }
}
