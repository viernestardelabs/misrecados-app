package com.viernestardelabs.misrecados.client.activities.profile;

import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.viernestardelabs.misrecados.client.activities.DetailView;

public interface ProfileView extends DetailView {
	
	public String getName();
	
	public void setName(String name);
	
	public String getContact();
	
	public void setContact(String contact);
	
	public Integer getLevel();
	
	public void setLevel(Integer level);

    HasTapHandlers getSaveButton();
}
