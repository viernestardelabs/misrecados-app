package com.viernestardelabs.misrecados.client.activities.navigation;

import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import java.util.List;

import com.google.gwt.user.client.ui.IsWidget;
import com.googlecode.mgwt.ui.client.widget.list.celllist.HasCellSelectedHandler;
import com.viernestardelabs.misrecados.client.activities.commons.Topic;

public interface NavigationView extends IsWidget {

	public void setTitle(String text);

    HasTapHandlers getAboutButton();

    public HasCellSelectedHandler getCellSelectedHandler();

	public void setTopics(List<Topic> createTopicsList);

	public void selectTopicByIndex(int index, boolean selected);
	
	public void unSelectTopic();
	
}
