package com.viernestardelabs.misrecados.client.activities;

import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.googlecode.mgwt.ui.client.MGWT;
import com.googlecode.mgwt.ui.client.widget.button.image.PreviousitemImageButton;
import com.googlecode.mgwt.ui.client.widget.header.HeaderPanel;
import com.googlecode.mgwt.ui.client.widget.header.HeaderTitle;
import com.googlecode.mgwt.ui.client.widget.panel.flex.FixedSpacer;
import com.googlecode.mgwt.ui.client.widget.panel.flex.FlexSpacer;
import com.googlecode.mgwt.ui.client.widget.panel.flex.RootFlexPanel;
import com.googlecode.mgwt.ui.client.widget.panel.scroll.ScrollPanel;

public class DetailViewGwtImpl implements DetailView {
    protected RootFlexPanel main;
    protected ScrollPanel scrollPanel;
    protected HeaderPanel headerPanel;
    protected PreviousitemImageButton headerBackButton;
    protected HeaderTitle title;

    public DetailViewGwtImpl() {
        main = new RootFlexPanel();

        scrollPanel = new ScrollPanel();

        headerPanel = new HeaderPanel();

        headerBackButton = new PreviousitemImageButton();

        headerPanel.add(getLeftActions());
        headerPanel.add(new FlexSpacer());
        headerPanel.add(title = new HeaderTitle());
        headerPanel.add(new FlexSpacer());
        headerPanel.add(getRightActions());

        main.add(headerPanel);
        main.add(scrollPanel);
    }

    protected Widget getLeftActions() {
        final boolean phoneNotAndroid = MGWT.getFormFactor().isPhone() && !MGWT.getOsDetection().isAndroid();
        return phoneNotAndroid ? headerBackButton : new FixedSpacer();
    }

    protected Widget getRightActions() {
        return new FixedSpacer();
    }

    @Override
    public Widget asWidget() {
        return main;
    }

    @Override
    public HasText getHeader() {
        return title;
    }

    @Override
    public HasTapHandlers getBackButton() {
        return headerBackButton;
    }
}
