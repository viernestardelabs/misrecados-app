package com.viernestardelabs.misrecados.client.activities.about;

import com.google.gwt.user.client.TakesValue;
import com.googlecode.gwtphonegap.client.device.Device;
import com.viernestardelabs.misrecados.client.activities.DetailView;

public interface AboutView extends DetailView {

    TakesValue<Device> getDevice();

    TakesValue<Boolean> getPhoneGapDeviceEnabled();

}
