package com.viernestardelabs.misrecados.client.activities.about;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.googlecode.gwtphonegap.client.device.Device;
import javax.inject.Inject;

public class DeviceEditor extends Composite implements Editor<Device> {
    interface DeviceUiBinder extends UiBinder<HTMLPanel, DeviceEditor> {}

    protected @UiField InlineLabel version;
    protected @UiField InlineLabel name;
    protected @UiField InlineLabel phoneGapVersion;
    protected @UiField InlineLabel platform;
    protected @UiField InlineLabel uuid;

    @Inject
    public DeviceEditor(DeviceUiBinder deviceUiBinder) {
        initWidget(deviceUiBinder.createAndBindUi(this));
    }
}
