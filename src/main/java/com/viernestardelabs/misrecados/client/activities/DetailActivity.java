package com.viernestardelabs.misrecados.client.activities;

import static com.viernestardelabs.misrecados.client.event.ActionNames.BACK;

import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.web.bindery.event.shared.EventBus;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.googlecode.mgwt.mvp.client.MGWTAbstractActivity;
import com.viernestardelabs.misrecados.client.event.ActionEvent;

public class DetailActivity extends MGWTAbstractActivity {

    protected final DetailView detailView;

    public DetailActivity(DetailView detailView) {
        this.detailView = detailView;
    }

    @Override
    public void start(AcceptsOneWidget panel, final EventBus eventBus) {
        addHandlerRegistration(detailView.getBackButton().addTapHandler(new TapHandler() {
            @Override
            public void onTap(TapEvent event) {
                ActionEvent.fire(eventBus, BACK);
            }
        }));
    }

}
