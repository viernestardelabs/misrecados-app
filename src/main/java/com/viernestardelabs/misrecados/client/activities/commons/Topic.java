package com.viernestardelabs.misrecados.client.activities.commons;

import com.google.gwt.place.shared.Place;

public class Topic {

    private final String name;
    private final Place place;

    public Topic(String name, Place place) {
        this.name = name;
        this.place = place;
    }

    public Place getPlace() {
        return place;
    }

    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((place == null) ? 0 : place.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;

        Topic other = (Topic) obj;
        if (place == null) {
            if (other.place != null) return false;
        } else if (!place.getClass().equals(other.place.getClass()))
            return false;
        return true;
    }

}
