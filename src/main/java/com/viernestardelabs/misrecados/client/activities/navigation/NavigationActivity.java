package com.viernestardelabs.misrecados.client.activities.navigation;

import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.googlecode.mgwt.mvp.client.MGWTAbstractActivity;
import com.googlecode.mgwt.ui.client.widget.list.celllist.CellSelectedEvent;
import com.googlecode.mgwt.ui.client.widget.list.celllist.CellSelectedHandler;
import com.viernestardelabs.misrecados.client.activities.about.AboutPlace;
import com.viernestardelabs.misrecados.client.activities.commons.Topic;
import com.viernestardelabs.misrecados.client.activities.errandlist.ErrandListPlace;
import com.viernestardelabs.misrecados.client.activities.profile.ProfilePlace;

public class NavigationActivity extends MGWTAbstractActivity {

    private static final List<Topic> TOPICS = ImmutableList.of(
            new Topic(ErrandListPlace.class.getSimpleName(), new ErrandListPlace()),
            new Topic(ProfilePlace.class.getSimpleName(), new ProfilePlace()));

    private final NavigationView navigationView;
    private final PlaceController placeController;

    @Inject
    public NavigationActivity(final NavigationView navigationView, final PlaceController placeController) {
        this.navigationView = navigationView;
        this.placeController = placeController;
    }

    @Override
    public void start(AcceptsOneWidget panel, final EventBus eventBus) {
        navigationView.setTitle("Mis Recados");
        navigationView.setTopics(TOPICS);
        
        addHandlerRegistration(navigationView.getCellSelectedHandler().addCellSelectedHandler(new CellSelectedHandler() {
            @Override
            public void onCellSelected(CellSelectedEvent event) {
                int index = event.getIndex();
            	navigationView.selectTopicByIndex(index, true);
				placeController.goTo(TOPICS.get(index).getPlace());
            }
        }));
        
        addHandlerRegistration(navigationView.getAboutButton().addTapHandler(new TapHandler() {
			@Override
			public void onTap(TapEvent event) {
				placeController.goTo(new AboutPlace());
			}
		}));

        panel.setWidget(navigationView);
    }
}
