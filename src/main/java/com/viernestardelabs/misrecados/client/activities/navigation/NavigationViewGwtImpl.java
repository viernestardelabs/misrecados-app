package com.viernestardelabs.misrecados.client.activities.navigation;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.googlecode.mgwt.ui.client.widget.button.image.AboutImageButton;
import com.googlecode.mgwt.ui.client.widget.header.HeaderPanel;
import com.googlecode.mgwt.ui.client.widget.header.HeaderTitle;
import com.googlecode.mgwt.ui.client.widget.list.celllist.BasicCell;
import com.googlecode.mgwt.ui.client.widget.list.celllist.CellList;
import com.googlecode.mgwt.ui.client.widget.list.celllist.HasCellSelectedHandler;
import com.googlecode.mgwt.ui.client.widget.panel.flex.FixedSpacer;
import com.googlecode.mgwt.ui.client.widget.panel.flex.FlexPanel;
import com.googlecode.mgwt.ui.client.widget.panel.flex.FlexSpacer;
import com.googlecode.mgwt.ui.client.widget.panel.flex.RootFlexPanel;
import com.googlecode.mgwt.ui.client.widget.panel.scroll.ScrollPanel;
import com.viernestardelabs.misrecados.client.PlaceConstants;
import com.viernestardelabs.misrecados.client.activities.commons.Topic;
import java.util.List;

public class NavigationViewGwtImpl implements NavigationView {

    private final FlexPanel main;
    private final HeaderTitle headerPanelTitle;
    private final CellList<Topic> cellList;
    private final AboutImageButton aboutImageButton;
    private int oldIndex;

    public NavigationViewGwtImpl() {
        main = new RootFlexPanel();

        headerPanelTitle = new HeaderTitle();
        aboutImageButton = new AboutImageButton();

        final HeaderPanel headerPanel = new HeaderPanel();
        headerPanel.add(new FixedSpacer());
        headerPanel.add(new FlexSpacer());
        headerPanel.add(headerPanelTitle);
        headerPanel.add(new FlexSpacer());
        headerPanel.add(aboutImageButton);
        main.add(headerPanel);

        cellList = new CellList<>(new BasicCell<Topic>() {
            @Override
            public String getDisplayString(Topic model) {
                return PlaceConstants.Lookup.tryLookup(model.getName());
            }

            @Override
            public boolean canBeSelected(Topic model) {
                return true;
            }
        });

        FlowPanel container = new FlowPanel();
        container.add(cellList);

        ScrollPanel scrollPanel = new ScrollPanel();
        scrollPanel.setWidget(container);
        scrollPanel.setScrollingEnabledX(false);
        main.add(scrollPanel);
    }

    @Override
    public Widget asWidget() {
        return main;
    }

    @Override
    public void setTitle(String text) {
        headerPanelTitle.setText(text);
    }

    @Override
    public HasTapHandlers getAboutButton() {
        return aboutImageButton;
    }

    @Override
    public HasCellSelectedHandler getCellSelectedHandler() {
        return cellList;
    }

    @Override
    public void setTopics(List<Topic> createTopicsList) {
        cellList.render(createTopicsList);
    }

    @Override
    public void selectTopicByIndex(int index, boolean selected) {
        unSelectTopic();
        cellList.setSelectedIndex(index, selected);
        oldIndex = index;
    }

    @Override
    public void unSelectTopic() {
        cellList.setSelectedIndex(oldIndex, false);
    }

}
