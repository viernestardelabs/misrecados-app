package com.viernestardelabs.misrecados.client.activities.about;

import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.user.client.TakesValue;
import com.google.gwt.user.client.ui.HTML;
import com.googlecode.gwtphonegap.client.device.Device;
import com.googlecode.mgwt.ui.client.widget.panel.Panel;
import com.googlecode.mgwt.ui.client.widget.panel.flex.FlexPanel;
import com.googlecode.mgwt.ui.client.widget.panel.flex.FlexPropertyHelper.Alignment;
import com.googlecode.mgwt.ui.client.widget.panel.flex.FlexPropertyHelper.Orientation;
import com.viernestardelabs.misrecados.client.activities.DetailViewGwtImpl;
import javax.inject.Inject;

public class AboutViewGwtImpl extends DetailViewGwtImpl implements AboutView {
    interface DeviceDriver extends SimpleBeanEditorDriver<Device, DeviceEditor> {}

    private final DeviceDriver deviceDriver;
    private final HTML buildMessage;
    public Boolean phoneGapDeviceEnabled;

    @Inject
    public AboutViewGwtImpl(DeviceEditor deviceEditor, DeviceDriver deviceDriver) {
        this.deviceDriver = deviceDriver;
        this.phoneGapDeviceEnabled = false;
        this.buildMessage = new HTML(getBuildMessage());
        deviceDriver.initialize(deviceEditor);
        Panel round = new Panel();

        FlexPanel flexPanel = new FlexPanel();
        flexPanel.setOrientation(Orientation.VERTICAL);
        flexPanel.setAlignment(Alignment.CENTER);
        round.add(flexPanel);
        round.setRound(true);

        flexPanel.add(new HTML("<b>MisRecados</b>"));
        flexPanel.add(new HTML("Version SNAPSHOT"));
        flexPanel.add(new HTML("Built by <i>Viernes Tarde Labs</i>, " +
                "<a target='_blank' href='http://www.twitter.com/viernestardelabs'>@viernestardelabs</a> on Twitter"));
        flexPanel.add(buildMessage);
        flexPanel.add(new HTML("<br/>"));

        flexPanel.add(deviceEditor);

        flexPanel.add(new HTML(
                "<br/><br/><a target='_blank' href='http://www.misrecados.com'>www.misrecados.com</a><br/><br/>"));

        scrollPanel.setWidget(round);
        scrollPanel.setScrollingEnabledX(false);
    }

    protected String getBuildMessage() {
        return "Using GWT " + (phoneGapDeviceEnabled ? "+ PhoneGap" : "") + " to build mobile apps";
    }

    @Override
    public TakesValue<Device> getDevice() {
        return new TakesValue<Device>() {
            @Override
            public Device getValue() {
                return deviceDriver.flush();
            }

            @Override
            public void setValue(Device device) {
                deviceDriver.edit(device);
                buildMessage.setHTML(getBuildMessage());
            }
        };
    }

    @Override
    public TakesValue<Boolean> getPhoneGapDeviceEnabled() {
        return new TakesValue<Boolean>() {
            @Override
            public void setValue(Boolean value) {
                phoneGapDeviceEnabled = value;
            }

            @Override
            public Boolean getValue() {
                return phoneGapDeviceEnabled;
            }
        };
    }
}
