package com.viernestardelabs.misrecados.client.activities.erranddetail;

import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.viernestardelabs.misrecados.client.activities.DetailActivity;

public class ErrandDetailActivity extends DetailActivity {

    private final ErrandDetailView errandDetailView;

    @Inject
    public ErrandDetailActivity(final ErrandDetailView errandDetailView) {
        super(errandDetailView);
        this.errandDetailView = errandDetailView;
    }

    @Override
    public void start(AcceptsOneWidget panel, final EventBus eventBus) {
        panel.setWidget(errandDetailView);
    }

}
