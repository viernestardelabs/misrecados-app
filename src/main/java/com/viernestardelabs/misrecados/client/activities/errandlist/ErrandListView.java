package com.viernestardelabs.misrecados.client.activities.errandlist;

import java.util.List;

import com.google.gwt.event.logical.shared.HasSelectionHandlers;
import com.google.gwt.user.client.Random;
import com.googlecode.mgwt.ui.client.widget.list.celllist.GroupingCellList;
import com.viernestardelabs.misrecados.client.activities.DetailView;

public interface ErrandListView extends DetailView {
    
    public void render(List<GroupingCellList.CellGroup<Header, Content>> models);
    
    public HasSelectionHandlers<Content> getHasSelectionHandlers();
    
    public class Header {
        private final String name;
        private final Integer numberErrands;

        public Header(String name, Integer numberErrands) {
            this.name = name;
            this.numberErrands = numberErrands;
        }

        public String getName() {
            return name;
        }
        
		public Integer getNumberErrands() {
			return numberErrands;
		}
    }

    public class Content {

        private final String name;
        private final String enterprise;
        private final String date;
        private String price;
        private final String address = "direccion " + Random.nextInt();
        private final String telephone = Integer.toString(Random.nextInt(1000000000));

		public Content(String name, String enterprise, String date) {
            this.name = name;
            this.enterprise = enterprise;
            this.date = date;
            if (Random.nextDouble() > 0.5) {
            	this.price = Double.toString(Random.nextDouble() * 1000) + (Random.nextBoolean() ? "$" : "€");
            } else {
            	this.price = null;
            }
        }

        public String getName() {
            return name;
        }

        public String getEnterprise() {
        	return enterprise;
        }
        
        public String getDate() {
        	return date;
        }

		public String getPrice() {
			if (price == null) {
				price = "";
			}
			return price;
		}

		public String getAddress() {
			return address;
		}

		public String getTelephone() {
			return telephone;
		}
    }
}
