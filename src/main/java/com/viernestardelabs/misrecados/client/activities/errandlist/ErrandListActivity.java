package com.viernestardelabs.misrecados.client.activities.errandlist;

import static com.viernestardelabs.misrecados.client.PlaceConstants.Lookup.tryLookup;

import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.googlecode.mgwt.ui.client.widget.list.celllist.GroupingCellList.CellGroup;
import com.googlecode.mgwt.ui.client.widget.list.celllist.GroupingCellList.StandardCellGroup;
import com.viernestardelabs.misrecados.client.activities.DetailActivity;
import com.viernestardelabs.misrecados.client.activities.erranddetail.ErrandDetailPlace;
import com.viernestardelabs.misrecados.client.activities.erranddetail.ErrandDetailView;
import com.viernestardelabs.misrecados.client.activities.errandlist.ErrandListView.Content;
import com.viernestardelabs.misrecados.client.activities.errandlist.ErrandListView.Header;
import com.viernestardelabs.misrecados.client.activities.navigation.HomePlace;
import com.viernestardelabs.misrecados.client.activities.navigation.NavigationView;

public class ErrandListActivity extends DetailActivity {
	
	private final NavigationView navigationView;
	private final ErrandDetailView errandDetailView;
    private final ErrandListView errandListView;
    private final PlaceController placeController;

    @Inject
    public ErrandListActivity(NavigationView navigationView, final ErrandDetailView errandDetailView, final ErrandListView errandListView, PlaceController placeController) {
        super(errandListView);
        this.navigationView = navigationView;
        this.errandDetailView = errandDetailView;
        this.errandListView = errandListView;
        this.placeController = placeController;
    }

    @Override
    public void start(AcceptsOneWidget panel, final EventBus eventBus) {
        super.start(panel, eventBus);
        errandListView.getHeader().setText(tryLookup(ErrandListPlace.class.getSimpleName()));
        errandListView.render(buildList());
        
        addHandlerRegistration(errandDetailView.getBackButton().addTapHandler(new TapHandler() {
			@Override
			public void onTap(TapEvent event) {
				navigationView.unSelectTopic();
				placeController.goTo(new HomePlace());
			}
		}));
        
        addHandlerRegistration(errandListView.getHasSelectionHandlers().addSelectionHandler(new SelectionHandler<Content>() {
            @Override
            public void onSelection(SelectionEvent<Content> event) {
            	navigationView.unSelectTopic();
                errandDetailView.setContentDetail(event.getSelectedItem());
                placeController.goTo(new ErrandDetailPlace());
            }
        }));
        
        panel.setWidget(errandListView);
    }

    private List<CellGroup<Header, Content>> buildList() {
        final List<Content> pendingErrandList = ImmutableList.of(
                new Content("Pending errand 1", "enterprise1", "date1"),
                new Content("Pending errand 2", "enterprise2", "date2"),
                new Content("Pending errand 3", "enterprise3", "date3"),
                new Content("Pending errand 4", "enterprise4", "date4"),
                new Content("Pending errand 5", "enterprise5", "date5"),
                new Content("Pending errand 6", "enterprise6", "date6")
        );
        final List<Content> doneErrandList = ImmutableList.of(
                new Content("Done errand 1", "enterprise1", "date1"),
                new Content("Done errand 2", "enterprise2", "date2"),
                new Content("Done errand 3", "enterprise3", "date3"),
                new Content("Done errand 4", "enterprise4", "date4"),
                new Content("Done errand 5", "enterprise5", "date5"),
                new Content("Done errand 6", "enterprise6", "date6"),
                new Content("Done errand 7", "enterprise7", "date7"),
                new Content("Done errand 8", "enterprise8", "date8"),
                new Content("Done errand 9", "enterprise9", "date9")
        );
        final Header pendingGroup = new Header("Pending errands", pendingErrandList.size());
        final Header doneGroup = new Header("Done errands", doneErrandList.size());
        return ImmutableList.<CellGroup<Header, Content>>of(
                new StandardCellGroup<>("pending", pendingGroup, pendingErrandList),
                new StandardCellGroup<>("done", doneGroup, doneErrandList));
    }
}
