package com.viernestardelabs.misrecados.client.activities.about;

import static com.viernestardelabs.misrecados.client.event.ActionNames.BACK;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;
import com.googlecode.gwtphonegap.client.PhoneGap;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.viernestardelabs.misrecados.client.activities.DetailActivity;
import com.viernestardelabs.misrecados.client.event.ActionEvent;

public class AboutActivity extends DetailActivity {
    private final AboutView aboutView;
    private final PhoneGap phoneGap;

    @Inject
    public AboutActivity(AboutView aboutView, PhoneGap phoneGap) {
        super(aboutView);
        this.aboutView = aboutView;
        this.phoneGap = phoneGap;
    }

    @Override
    public void start(AcceptsOneWidget panel, final EventBus eventBus) {
        addHandlerRegistration(detailView.getBackButton().addTapHandler(new TapHandler() {
            @Override
            public void onTap(TapEvent event) {
                ActionEvent.fire(eventBus, BACK);
            }
        }));
        aboutView.getHeader().setText("About");
        panel.setWidget(aboutView);
        aboutView.getPhoneGapDeviceEnabled().setValue(phoneGap.isPhoneGapDevice());
        aboutView.getDevice().setValue(phoneGap.getDevice());
    }
}
