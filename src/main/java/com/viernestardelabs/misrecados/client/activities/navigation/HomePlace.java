package com.viernestardelabs.misrecados.client.activities.navigation;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class HomePlace extends Place {
    public static class HomePlaceTokenizer implements PlaceTokenizer<HomePlace> {
        @Override
        public HomePlace getPlace(String token) {
            return new HomePlace();
        }

        @Override
        public String getToken(HomePlace place) {
            return "";
        }
    }

    @Override
    public int hashCode() {
        return HomePlace.class.hashCode();
    }

    @Override
    public boolean equals(Object other) {
        return other == this || other instanceof HomePlace;
    }

    @Override
    public String toString() {
        return "home";
    }
}
