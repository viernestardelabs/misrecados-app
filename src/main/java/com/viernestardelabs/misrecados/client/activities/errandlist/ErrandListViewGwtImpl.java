package com.viernestardelabs.misrecados.client.activities.errandlist;

import java.util.List;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.logical.shared.HasSelectionHandlers;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.ImageResourceRenderer;
import com.google.gwt.user.client.ui.Label;
import com.googlecode.mgwt.ui.client.widget.list.celllist.Cell;
import com.googlecode.mgwt.ui.client.widget.list.celllist.GroupingCellList;
import com.googlecode.mgwt.ui.client.widget.list.celllist.HeaderList;
import com.googlecode.mgwt.ui.client.widget.panel.flex.FlexPanel;
import com.viernestardelabs.misrecados.client.activities.DetailViewGwtImpl;
import com.viernestardelabs.misrecados.client.bundle.ClientResources;

public class ErrandListViewGwtImpl extends DetailViewGwtImpl implements ErrandListView {
    private final HeaderList<Header, Content> headerList;
	private final Image cartImage;
	private final FlexPanel summaryPanel;
	private final Label errandsLeftLabel;
	private final GroupingCellList<Header, Content> selectableCellGroup;

    public ErrandListViewGwtImpl() {
    	scrollPanel.removeFromParent();
    	
    	ClientResources resources = GWT.create(ClientResources.class);
    	
    	summaryPanel = new FlexPanel();
    	cartImage = new Image(resources.cartImage());
    	errandsLeftLabel = new Label("3 errands left in the next 4 hours");
    	selectableCellGroup = new GroupingCellList<>(new ContentCell(), new HeaderCell());
		headerList = new HeaderList<>(selectableCellGroup);
    	
    	configureErrandListView();
    }

	private void configureErrandListView() {
		Style summaryStyle = summaryPanel.getElement().getStyle();
		summaryStyle.setMargin(20, Unit.PX);
		summaryPanel.add(cartImage);
		summaryPanel.add(errandsLeftLabel);
		main.add(summaryPanel);
		main.add(headerList);
	}

    private static class ContentCell implements Cell<Content> {
    	
    	private final ImageResourceRenderer imageRenderer;
		private final ClientResources resources;
    	
    	public ContentCell() {
    		imageRenderer = new ImageResourceRenderer();
    		resources = GWT.create(ClientResources.class);
    	}
    	
        @Override
        public void render(SafeHtmlBuilder safeHtmlBuilder, Content model) {
			safeHtmlBuilder.appendEscaped(model.getName());
			safeHtmlBuilder.appendEscaped(" " + model.getEnterprise() + " ");
			safeHtmlBuilder.append(imageRenderer.render(resources.dateIcon()));
			safeHtmlBuilder.appendEscaped(" " + model.getDate());
			if (model.getPrice() != null) {
				safeHtmlBuilder.appendEscaped(" " + model.getPrice());
				safeHtmlBuilder.append(imageRenderer.render(resources.priceIcon()));
				safeHtmlBuilder.appendEscaped(" ");
			}
			safeHtmlBuilder.append(imageRenderer.render(resources.chatIcon()));
        }

        @Override
        public boolean canBeSelected(Content model) {
            return true;
        }
    }

    private static class HeaderCell implements Cell<Header> {
        @Override
        public void render(SafeHtmlBuilder safeHtmlBuilder, Header model) {
            safeHtmlBuilder.appendEscaped(model.getName());
            safeHtmlBuilder.appendEscaped(" " + model.getNumberErrands() + " errands");
        }

        @Override
        public boolean canBeSelected(Header model) {
            return false;
        }
    }

    @Override
    public void render(List<GroupingCellList.CellGroup<Header, Content>> models) {
        headerList.render(models);
    }

	@Override
	public HasSelectionHandlers<Content> getHasSelectionHandlers() {
		return selectableCellGroup;
	}
}
