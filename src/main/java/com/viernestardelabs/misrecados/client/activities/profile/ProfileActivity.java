package com.viernestardelabs.misrecados.client.activities.profile;

import static com.viernestardelabs.misrecados.client.PlaceConstants.Lookup.tryLookup;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.web.bindery.event.shared.EventBus;
import com.googlecode.gwtphonegap.client.notification.ConfirmCallback;
import com.googlecode.gwtphonegap.client.notification.Notification;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.viernestardelabs.misrecados.client.activities.DetailActivity;
import com.viernestardelabs.misrecados.client.services.ProfileRequest;
import com.viernestardelabs.misrecados.shared.domain.Profile;

public class ProfileActivity extends DetailActivity {

    private final ProfileView profileView;
    private final Provider<ProfileRequest> profileRequestProvider;
    private final Notification notification;

    @Inject
    public ProfileActivity(
            final ProfileView profileView,
            final Provider<ProfileRequest> profileRequestProvider,
            final Notification notification) {
        super(profileView);
        this.profileView = profileView;
        this.profileRequestProvider = profileRequestProvider;
        this.notification = notification;
    }

    @Override
    public void start(AcceptsOneWidget panel, final EventBus eventBus) {
        super.start(panel, eventBus);

        profileView.getHeader().setText(tryLookup(ProfilePlace.class.getSimpleName()));
        profileView.getSaveButton().addTapHandler(new TapHandler() {
            @Override
            public void onTap(TapEvent event) {
                String name = profileView.getName();
                String contact = profileView.getContact();
                Integer level = profileView.getLevel();
                final Profile profile = Profile.create(name, contact, level);
                notification.confirm("Do you want to save the changes you made?\n" + profile, new ConfirmCallback() {
                    @Override
                    public void onConfirm(int button) {
                        GWT.log("confirm result " + button);
                    }
                });
                profileRequestProvider.get().saveProfile(profile);
            }
        });

        Profile profile = profileRequestProvider.get().getMeProfile();
        profileView.setName(profile.getName());
        profileView.setContact(profile.getContact());
        profileView.setLevel(profile.getLevel());

        panel.setWidget(profileView);
    }

}
