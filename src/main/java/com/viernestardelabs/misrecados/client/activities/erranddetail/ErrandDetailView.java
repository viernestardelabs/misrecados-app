package com.viernestardelabs.misrecados.client.activities.erranddetail;

import com.viernestardelabs.misrecados.client.activities.DetailView;
import com.viernestardelabs.misrecados.client.activities.errandlist.ErrandListView.Content;

public interface ErrandDetailView extends DetailView {
	
	public void setContentDetail(Content detail);
}
