package com.viernestardelabs.misrecados.client.activities.profile;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.googlecode.mgwt.ui.client.MGWT;
import com.googlecode.mgwt.ui.client.widget.button.Button;
import com.googlecode.mgwt.ui.client.widget.form.Form;
import com.googlecode.mgwt.ui.client.widget.form.FormEntry;
import com.googlecode.mgwt.ui.client.widget.input.MTextBox;
import com.googlecode.mgwt.ui.client.widget.input.slider.Slider;
import com.viernestardelabs.misrecados.client.activities.DetailViewGwtImpl;
import com.viernestardelabs.misrecados.client.ui.HangoutTimeline;

public class ProfileViewGwtImpl extends DetailViewGwtImpl implements ProfileView {

    private final MTextBox nameBox = new MTextBox();
    private final MTextBox contactBox = new MTextBox();
    private final Slider levelSlider = new Slider();
    private Button saveButton;

    @Inject
    public ProfileViewGwtImpl(HangoutTimeline timeline) {
        super();

        FlowPanel container = new FlowPanel();

        final Form form = new Form();

        // lets put some fields
        form.add(new FormEntry("Name", nameBox));
        form.add(new FormEntry("Contact", contactBox));
        form.add(new FormEntry("level", levelSlider));
        form.add(new FormEntry("level", levelSlider));

        container.add(form);
        container.add(timeline);

        scrollPanel.setScrollingEnabledX(false);
        scrollPanel.setScrollingEnabledY(true);
        scrollPanel.setWidget(container);
        // workaround for android formFields jumping around when using -webkit-transform
        scrollPanel.setUsePos(MGWT.getOsDetection().isAndroid());
    }

    @Override
    protected Widget getRightActions() {
        if (saveButton == null) {
            // and the action button
            this.saveButton = new Button();
            this.saveButton.setText("Save");
        }
        return saveButton;
    }

    @Override
    public String getName() {
        return nameBox.getText();
    }

    @Override
    public void setName(String name) {
        nameBox.setText(name);
    }

    @Override
    public String getContact() {
        return contactBox.getText();
    }

    @Override
    public void setContact(String contact) {
        contactBox.setText(contact);
    }

    @Override
    public Integer getLevel() {
        return levelSlider.getValue();
    }

    @Override
    public void setLevel(Integer level) {
        levelSlider.setValue(level);
    }

    @Override
    public HasTapHandlers getSaveButton() {
        return saveButton;
    }

}
