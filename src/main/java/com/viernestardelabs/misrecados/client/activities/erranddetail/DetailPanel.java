package com.viernestardelabs.misrecados.client.activities.erranddetail;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.viernestardelabs.misrecados.client.activities.errandlist.ErrandListView.Content;
import com.viernestardelabs.misrecados.client.bundle.ClientResources;

public class DetailPanel extends Composite {

	private static LoginUiBinder uiBinder = GWT.create(LoginUiBinder.class);
	
	@UiTemplate("DetailPanel.ui.xml")
	interface LoginUiBinder extends UiBinder<Widget, DetailPanel> {
	}

	@UiField(provided = true)
	final ClientResources res;
	@UiField
	Label enterpriseLabel;
	@UiField
	Label deadlineTimeLabel;
	@UiField
	Label priceLabel;
	@UiField
	Label addressLabel;
	@UiField
	Label telephoneLabel;

	public DetailPanel() {
		this.res = GWT.create(ClientResources.class);
		res.style().ensureInjected();
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	public void refreshData(Content data) {
		enterpriseLabel.setText(data.getEnterprise());
		deadlineTimeLabel.setText(data.getDate());
		if (data.getPrice() != null) {
			priceLabel.setText(data.getPrice());
		}
		addressLabel.setText(data.getAddress());
		telephoneLabel.setText(data.getTelephone());
	}
}
