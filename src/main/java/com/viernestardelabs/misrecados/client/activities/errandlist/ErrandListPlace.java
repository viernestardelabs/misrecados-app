package com.viernestardelabs.misrecados.client.activities.errandlist;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class ErrandListPlace extends Place {
	public static class ErrandListPlaceTokenizer implements PlaceTokenizer<ErrandListPlace> {
		@Override
		public ErrandListPlace getPlace(String token) {
			return new ErrandListPlace();
		}

		@Override
		public String getToken(ErrandListPlace place) {
			return "";
		}
	}

    @Override
    public String toString() {
        return "errand-list";
    }
}
