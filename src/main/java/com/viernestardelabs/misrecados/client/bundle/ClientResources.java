package com.viernestardelabs.misrecados.client.bundle;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.ImageResource;

public interface ClientResources extends ClientBundle {
	public interface MyCss extends CssResource {
		String blackText();
	}

	@Source("DetailPanel.css")
	MyCss style();
	
	@Source("return_purchase-128.png")
	ImageResource cartImage();
	
	@Source("google_maps.png")
	ImageResource mapImage();
	
	@Source("banknotes-128 2.png")
	ImageResource priceIcon();
	
	@Source("watch-128.png")
	ImageResource dateIcon();
	
	@Source("quote-128.png")
	ImageResource chatIcon();
	
	@Source("marker-64.png")
	ImageResource locationPointIcon();
	
	@Source("map_marker-64.png")
	ImageResource addressIcon();
	
	@Source("phone1-75.png")
	ImageResource telephoneIcon();

}
