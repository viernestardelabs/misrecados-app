package com.viernestardelabs.misrecados.client.inject;

import com.google.gwt.core.shared.GWT;

public interface GinjectorProvider {
    ClientGinjector get();

    class TabletGinjectorProvider implements GinjectorProvider {
        public ClientGinjector get() {
            return GWT.create(ClientTabletGinjector.class);
        }
    }

    class PhoneGinjectorProvider implements GinjectorProvider {
        public ClientGinjector get() {
            return GWT.create(ClientPhoneGinjector.class);
        }
    }
}
