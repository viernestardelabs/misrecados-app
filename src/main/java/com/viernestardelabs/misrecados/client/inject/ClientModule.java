package com.viernestardelabs.misrecados.client.inject;

import com.google.gwt.inject.client.AbstractGinModule;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.place.shared.PlaceHistoryMapper;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.SimpleEventBus;
import com.googlecode.gwtphonegap.client.PhoneGap;
import com.googlecode.mgwt.mvp.client.history.HistoryObserver;
import com.googlecode.mgwt.mvp.client.history.MGWTPlaceHistoryHandler;
import com.viernestardelabs.misrecados.client.AppHistoryObserver;
import com.viernestardelabs.misrecados.client.AppPlaceHistoryMapper;
import com.viernestardelabs.misrecados.client.MisRecadosEntryPoint;
import com.viernestardelabs.misrecados.client.activities.about.AboutView;
import com.viernestardelabs.misrecados.client.activities.about.AboutViewGwtImpl;
import com.viernestardelabs.misrecados.client.activities.erranddetail.ErrandDetailView;
import com.viernestardelabs.misrecados.client.activities.erranddetail.ErrandDetailViewGwtImpl;
import com.viernestardelabs.misrecados.client.activities.errandlist.ErrandListView;
import com.viernestardelabs.misrecados.client.activities.errandlist.ErrandListViewGwtImpl;
import com.viernestardelabs.misrecados.client.activities.navigation.HomePlace;
import com.viernestardelabs.misrecados.client.activities.navigation.NavigationView;
import com.viernestardelabs.misrecados.client.activities.navigation.NavigationViewGwtImpl;
import com.viernestardelabs.misrecados.client.activities.profile.ProfileView;
import com.viernestardelabs.misrecados.client.activities.profile.ProfileViewGwtImpl;
import com.viernestardelabs.misrecados.client.services.ProfileRequest;
import com.viernestardelabs.misrecados.client.services.impl.DummyProfileRequest;

public class ClientModule extends AbstractGinModule {
    @Override
    protected void configure() {
        bind(EventBus.class).to(SimpleEventBus.class).in(Singleton.class);
        bind(NavigationView.class).to(NavigationViewGwtImpl.class).in(Singleton.class);
        bind(ErrandListView.class).to(ErrandListViewGwtImpl.class).in(Singleton.class);
        bind(ErrandDetailView.class).to(ErrandDetailViewGwtImpl.class).in(Singleton.class);
        bind(ProfileView.class).to(ProfileViewGwtImpl.class).in(Singleton.class);
        bind(PlaceHistoryMapper.class).to(AppPlaceHistoryMapper.class).in(Singleton.class);
        bind(HistoryObserver.class).to(AppHistoryObserver.class).in(Singleton.class);
        bind(AboutView.class).to(AboutViewGwtImpl.class).in(Singleton.class);
    }

    @Provides
    @Singleton
    public PlaceController providePlaceController(EventBus eventBus) {
        return new PlaceController(eventBus);
    }

    @Provides
    @Singleton
    public ProfileRequest provideProfileRequest() {
        return DummyProfileRequest.instance();
    }

    @Provides
    @Singleton
    public MGWTPlaceHistoryHandler provideMgwtPlaceHistoryHandler(PlaceController placeController, EventBus eventBus,
            PlaceHistoryMapper historyMapper, HistoryObserver historyObserver) {
        final MGWTPlaceHistoryHandler historyHandler = new MGWTPlaceHistoryHandler(historyMapper, historyObserver);
        historyHandler.register(placeController, eventBus, new HomePlace());
        return historyHandler;
    }

    @Provides
    @Singleton
    private PhoneGap providePhoneGap() {
        return MisRecadosEntryPoint.phoneGap;
    }
}
