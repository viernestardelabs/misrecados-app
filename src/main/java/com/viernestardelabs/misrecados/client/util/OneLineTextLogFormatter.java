package com.viernestardelabs.misrecados.client.util;

import com.google.gwt.logging.impl.FormatterImpl;
import com.google.gwt.logging.impl.StackTracePrintStream;
import java.util.logging.Handler;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/** One line text log formatter, because change matters and two line formatter. */
public class OneLineTextLogFormatter extends FormatterImpl {
    public static void install() {
        Logger logger = Logger.getLogger("");
        if (logger != null && logger.getHandlers() != null) {
            for (Handler handler : logger.getHandlers()) {
                handler.setFormatter(new OneLineTextLogFormatter());
            }
        }
    }

    @Override
    public String format(LogRecord event) {
        StringBuilder message = new StringBuilder();
        appendRecordInfo(event, message);
        message.append(event.getMessage());
        appendStackTraces(event, message);
        return message.toString();
    }

    private void appendRecordInfo(LogRecord event, StringBuilder message) {
        message.append(event.getLoggerName().replaceAll(".*\\.","")).append(": ");
    }

    private void appendStackTraces(LogRecord event, StringBuilder message) {
        final Throwable thrown = event.getThrown();
        if (thrown != null) appendStackTraces(message.append("\n"), thrown);
    }

    private void appendStackTraces(StringBuilder message, Throwable thrown) {
        thrown.printStackTrace(new StackTracePrintStream(message));
    }
}
