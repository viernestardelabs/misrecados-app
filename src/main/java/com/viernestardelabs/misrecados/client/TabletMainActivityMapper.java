package com.viernestardelabs.misrecados.client;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;
import com.google.inject.Inject;
import com.viernestardelabs.misrecados.client.activities.about.AboutActivity;
import com.viernestardelabs.misrecados.client.activities.about.AboutPlace;
import com.viernestardelabs.misrecados.client.activities.erranddetail.ErrandDetailActivity;
import com.viernestardelabs.misrecados.client.activities.erranddetail.ErrandDetailPlace;
import com.viernestardelabs.misrecados.client.activities.errandlist.ErrandListActivity;
import com.viernestardelabs.misrecados.client.activities.errandlist.ErrandListPlace;
import com.viernestardelabs.misrecados.client.activities.profile.ProfileActivity;
import com.viernestardelabs.misrecados.client.activities.profile.ProfilePlace;

import javax.annotation.Nullable;
import javax.inject.Singleton;

@Singleton
public class TabletMainActivityMapper implements ActivityMapper {

    private final ErrandDetailActivity errandDetailActivity;
    private final ErrandListActivity errandListActivity;
    private final ProfileActivity profileActivity;
    private final AboutActivity aboutActivity;

    @Inject
    public TabletMainActivityMapper(
            final ErrandDetailActivity errandDetailActivity,
            final ErrandListActivity errandListActivity,
            final ProfileActivity profileActivity, 
            final AboutActivity aboutActivity) {
        this.errandDetailActivity = errandDetailActivity;
        this.errandListActivity = errandListActivity;
        this.profileActivity = profileActivity;
        this.aboutActivity = aboutActivity;
    }

    @Override
    @Nullable
    public Activity getActivity(Place place) {
        if (place instanceof ErrandDetailPlace) return errandDetailActivity;
        if (place instanceof ErrandListPlace) return errandListActivity;
        if (place instanceof ProfilePlace) return profileActivity;
        if (place instanceof AboutPlace) return aboutActivity;
        return null;
    }

}
