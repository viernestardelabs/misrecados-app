package com.viernestardelabs.misrecados.client;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.core.client.GWT;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;
import com.googlecode.mgwt.mvp.client.AnimatingActivityManager;
import com.googlecode.mgwt.mvp.client.history.MGWTPlaceHistoryHandler;
import com.googlecode.mgwt.ui.client.MGWT;
import com.googlecode.mgwt.ui.client.MGWTSettings;
import com.googlecode.mgwt.ui.client.widget.animation.AnimationWidget;
import com.googlecode.mgwt.ui.client.widget.menu.overlay.OverlayMenu;

import java.util.logging.Logger;
import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Singleton;

public class ClientBootstrapper {
    private @Inject MGWTPlaceHistoryHandler historyHandler;
    private @Inject FormfactorBootstrapper formfactorBootstrapper;

    public void start() {
        MGWTSettings.ViewPort viewPort = new MGWTSettings.ViewPort();
        viewPort.setUserScaleAble(false).setMinimumScale(1.0).setMinimumScale(1.0).setMaximumScale(1.0);

        MGWTSettings settings = new MGWTSettings();
        settings.setViewPort(viewPort);
        settings.setIconUrl("logo.png");
        settings.setFullscreen(true);
        settings.setFixIOS71BodyBug(true);
        settings.setPreventScrolling(true);

        MGWT.applySettings(settings);

        RootPanel.get().add(formfactorBootstrapper.createDisplay());
        historyHandler.handleCurrentHistory();
    }

    public interface FormfactorBootstrapper {
        Widget createDisplay();
    }

    @Singleton
    public static class PhoneBootstrapper implements FormfactorBootstrapper {
        private @Inject EventBus eventBus;
        private @Inject PhoneActivityMapper phoneActivityMapper;
        private @Inject PhoneAnimationMapper phoneAnimationMapper;

        @Override
        public Widget createDisplay() {
            final AnimationWidget display = GWT.create(AnimationWidget.class);
            final AnimatingActivityManager activityManager = new AnimatingActivityManager(
                    LoggingActivityMapper.decorate(phoneActivityMapper), phoneAnimationMapper, eventBus);
            activityManager.setDisplay(display);
            return display;
        }

    }

    public static class TabletBootstrapper implements FormfactorBootstrapper {
        private @Inject EventBus eventBus;
        private @Inject TabletMainActivityMapper tabletMainActivityMapper;
        private @Inject TabletMainAnimationMapper tabletMainAnimationMapper;
        private @Inject TabletNavActivityMapper tabletNavActivityMapper;
        private @Inject TabletNavAnimationMapper tabletNavAnimationMapper;

        @Override
        public Widget createDisplay() {
            OverlayMenu overlayMenu = new OverlayMenu();
            overlayMenu.setMaster(createTabletNavContainer());
            overlayMenu.setDetail(createTabletMainContainer());
            return overlayMenu;
        }

        private Widget createTabletMainContainer() {
            final AnimationWidget mainDisplay = new AnimationWidget();
            final AnimatingActivityManager mainActivityManager = new AnimatingActivityManager(
                    LoggingActivityMapper.decorate(tabletMainActivityMapper), tabletMainAnimationMapper, eventBus);

            mainActivityManager.setDisplay(mainDisplay);
            return mainDisplay;
        }

        private Widget createTabletNavContainer() {
            final AnimationWidget navDisplay = new AnimationWidget();
            final AnimatingActivityManager navActivityManager = new AnimatingActivityManager(
                    LoggingActivityMapper.decorate(tabletNavActivityMapper), tabletNavAnimationMapper, eventBus);

            navActivityManager.setDisplay(navDisplay);
            return navDisplay;
        }
    }

    static class LoggingActivityMapper implements ActivityMapper {
        static LoggingActivityMapper decorate(ActivityMapper decorated) {
            return new LoggingActivityMapper(decorated);
        }

        private final ActivityMapper decorated;
        private final Logger logger;

        private LoggingActivityMapper(ActivityMapper decorated) {
            this.decorated = decorated;
            this.logger = Logger.getLogger(decorated.getClass().getName());
        }

        @Override
        public Activity getActivity(Place place) {
            final Activity activity = decorated.getActivity(place);
            logger.fine("resolved place " + getSimpleName(place) + " to activity " + getSimpleName(activity));
            return activity;
        }

        protected String getSimpleName(@Nullable Object object) {
            if (object == null) return "null";
            return object.getClass().getSimpleName();
        }
    }
}
