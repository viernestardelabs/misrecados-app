package com.viernestardelabs.misrecados.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.ConstantsWithLookup;

public interface PlaceConstants extends ConstantsWithLookup {

    @DefaultStringValue("Errand List")
    String ErrandListPlace();

    @DefaultStringValue("Navigation")
    String navigation();

    @DefaultStringValue("Errand Detail")
    String ErrandDetailPlace();

    @DefaultStringValue("Past activity")
    String past_activity();

    @DefaultStringValue("Profile")
    String ProfilePlace();

    @DefaultStringValue("Upcoming activity")
    String upcoming_activity();

    public static class Lookup {
        public static final PlaceConstants INSTANCE = GWT.create(PlaceConstants.class);

        public static String tryLookup(String placeToken) {
            try {
                return INSTANCE.getString(placeToken);
            } catch (Exception ignore) {
                return "?" + placeToken + "?";
            }
        }
    }

}
