package com.viernestardelabs.misrecados.client;

import static com.google.gwt.core.client.ScriptInjector.TOP_WINDOW;
import static com.google.gwt.query.client.plugins.deferred.Deferred.when;

import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.ScriptInjector;
import com.google.gwt.query.client.Function;
import com.google.gwt.query.client.Promise;
import com.google.gwt.query.client.plugins.deferred.Deferred;
import com.google.gwt.user.client.Window;
import com.googlecode.gwtphonegap.client.PhoneGap;
import com.googlecode.gwtphonegap.client.PhoneGapAvailableEvent;
import com.googlecode.gwtphonegap.client.PhoneGapAvailableHandler;
import com.googlecode.gwtphonegap.client.PhoneGapTimeoutEvent;
import com.googlecode.gwtphonegap.client.PhoneGapTimeoutHandler;
import com.viernestardelabs.misrecados.client.inject.ClientGinjector;
import com.viernestardelabs.misrecados.client.inject.GinjectorProvider;
import com.viernestardelabs.misrecados.client.util.OneLineTextLogFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MisRecadosEntryPoint implements EntryPoint {
    private static final Logger log = Logger.getLogger(MisRecadosEntryPoint.class.getName());
    public static final PhoneGap phoneGap = GWT.create(PhoneGap.class);
    private static final ClientGinjector ginjector = GWT.<GinjectorProvider> create(GinjectorProvider.class).get();

    @Override
    public void onModuleLoad() {
        OneLineTextLogFormatter.install();
        GWT.setUncaughtExceptionHandler(new GWT.UncaughtExceptionHandler() {
            @Override
            public void onUncaughtException(Throwable e) {
                Window.alert("uncaught: " + e.getLocalizedMessage());
                Window.alert(e.getMessage());
                log.log(Level.SEVERE, "uncaught exception", e);
            }
        });

        when(loadCordovaJs())
                .then(new Function() {
                    @Override
                    public Promise f(Object... args) {
                        return initializePhoneGap();
                    }
                })
                .done(new Function() {
                    @Override
                    public void f() {
                        ginjector.getClientBootstrapper().start();
                    }
                })
                .fail(new Function() {
                    @Override
                    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
                    public void f() {
                        Window.alert(this.<Exception> arguments(0).getLocalizedMessage());
                    }
                });
    }

    protected Promise loadCordovaJs() {
        final Deferred dfd = new Deferred();
        if (phoneGap.isPhoneGapDevice()) {
            final ScriptInjector.FromUrl cordovaJs = ScriptInjector.fromUrl("cordova.js").setWindow(TOP_WINDOW)
                    .setCallback(new Callback<Void, Exception>() {
                        @Override
                        public void onSuccess(Void result) {
                            log.info("cordova.js loaded");
                            dfd.resolve();
                        }

                        @Override
                        public void onFailure(Exception reason) {
                            dfd.reject(reason);
                        }
                    });
            log.info("loading cordova.js...");
            cordovaJs.inject();
        } else {
            dfd.resolve();
        }
        return dfd.promise();
    }

    protected Promise initializePhoneGap() {
        final Deferred dfd = new Deferred();
        phoneGap.addHandler(new PhoneGapAvailableHandler() {
            @Override
            public void onPhoneGapAvailable(PhoneGapAvailableEvent event) {
                log.info("phonegap available");
                dfd.resolve();
            }
        });
        phoneGap.addHandler(new PhoneGapTimeoutHandler() {
            @Override
            public void onPhoneGapTimeout(PhoneGapTimeoutEvent event) {
                dfd.reject(new Exception("can not load phonegap"));
            }
        });
        log.info("initializing phonegap...");
        phoneGap.initializePhoneGap();
        return dfd.promise();
    }
}
