package com.viernestardelabs.misrecados.shared.domain;

import static com.google.common.base.MoreObjects.toStringHelper;

public class Profile {
    public static Profile create(String name, String contact, Integer level) {
        Profile profile = new Profile();
        profile.setName(name);
        profile.setContact(contact);
        profile.setLevel(level);
        return profile;
    }

    private String name;
    private String contact;
    private Integer level;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return toStringHelper(this)
                .add("name", name)
                .add("contact", contact)
                .add("level", level)
                .toString();
    }
}
